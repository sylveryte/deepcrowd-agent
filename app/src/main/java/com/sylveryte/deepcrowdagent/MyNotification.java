package com.sylveryte.deepcrowdagent;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class MyNotification {
    public String cameraname;
    public String comment;
    public String count;
    public String time;

    public MyNotification() {
        // Default constructor required for calls to DataSnapshot.getValue(Notification.class)
    }

    public MyNotification(String cameraname, String comment, String count, String time) {
        this.cameraname = cameraname;
        this.comment = comment;
        this.count = count;
        this.time = time;
    }

    @Override
    public String toString() {
        return " camera :"+this.cameraname+
                " comment :"+this.comment+
                " count :"+this.count+
                " time :"+this.time
                ;
    }
}

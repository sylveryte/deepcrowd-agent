package com.sylveryte.deepcrowdagent;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class NotificationService extends Service {
    private DatabaseReference mSearchedLocationReference;
    private Random mRandom;
    NotificationManagerCompat mNotificationManager;

    public NotificationService() {
    }

    @Override
    public void onStart(Intent intent, int startid) {

        Toast.makeText(this, "Ninja is on mission", Toast.LENGTH_LONG).show();
        mRandom = new Random();
        initChannels(this);
        //show notification
        mNotificationManager = NotificationManagerCompat.from(NotificationService.this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid;
        if (user != null) {
            uid = user.getUid();


            mSearchedLocationReference = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child("users")
                    .child(uid);

            mSearchedLocationReference.limitToLast(1).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    MyNotification my_notification = dataSnapshot.getValue(MyNotification.class);
                    Log.d("TAG", "value: " + my_notification); //log

                    Notification notification = new NotificationCompat.Builder(NotificationService.this, "default")
                            .setSmallIcon(R.drawable.common_google_signin_btn_icon_light)
                            .setContentTitle(my_notification.cameraname)
                            .setContentText("Count->"+my_notification.count+" time->"+my_notification.time+" "+my_notification.comment)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .build();

                    int randomNumber = mRandom.nextInt(6005 - 1 ) + 6005;

                    // notificationId is a unique int for each my_notification that you must define
                    mNotificationManager.notify(randomNumber, notification);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("default",
                "Channel name",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Ninja is dead", Toast.LENGTH_LONG).show();
    }
}
